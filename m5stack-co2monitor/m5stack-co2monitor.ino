#include <M5Stack.h>
#include <HTTPClient.h>

#if defined(ESP32)
#include <WiFiMulti.h>
WiFiMulti wifiMulti;
#define DEVICE "ESP32"
#elif defined(ESP8266)
#include <ESP8266WiFiMulti.h>
ESP8266WiFiMulti wifiMulti;
#define DEVICE "ESP8266"
#endif

#define WIFI_SSID "SSID"
#define WIFI_PASS "PASS"
#define ENDPOINT "http://192.168.11.2:8086/api/v2/write?org=YOUR_ORG&bucket=YOUR_BUCKET&precision=s"
const char get_json[] = "co2 ppm=%d %ld";
int lcdOnOff = 1;   // 0: off, not 0: on

void lcdOff() {
  M5.Lcd.sleep();
  M5.Lcd.setBrightness(0);
}

void lcdOn() {
  M5.Lcd.wakeup();
  M5.Lcd.setBrightness(200);
}

void postServer(int ppm, time_t timestamp) {
  if (ppm == 0) {
    // do not send
    return;
  }
  char get_json_body[100] = "";
  sprintf(get_json_body, get_json, ppm, timestamp);

  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;
    http.begin(ENDPOINT);
    http.addHeader("Authorization", "Token YOUR_INFLUX_TOKEN");
    int http_code = http.POST((uint8_t*)get_json_body, strlen(get_json_body));

    if(http_code > 0) {
      M5.Lcd.printf("get status_code: %d", http_code);
    } else {
      M5.Lcd.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(http_code).c_str());
    }
    http.end();
  } else {
    M5.Lcd.println("not connect wifi");
  }
}

void setup() {
  // init lcd, serial, but don't init sd card
  M5.begin(true, false, true);
  M5.Power.begin();

  // LCD display
  M5.Lcd.clear(BLACK);
  M5.Lcd.setTextColor(YELLOW, BLACK);
  M5.Lcd.setTextSize(2);

  M5.Lcd.print("wifi setup");
  while (WiFi.status() != WL_CONNECTED) {
    WiFi.begin(WIFI_SSID, WIFI_PASS);
    delay(3000);
    M5.Lcd.printf(".");
  }

  const char* ntpServer = "ntp.nict.jp";
  const long  gmtOffset_sec = 3600 * 9;
  const int   daylightOffset_sec = 0;

  //init and get the time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);

  // Serial.begin(115200);
  // Serial.begin(38400, SERIAL_8N1, 3, 1);
  Serial2.begin(38400, SERIAL_8N1, 16, 17);
}

void loop() {
  M5.update();

  M5.Lcd.setCursor(0, 0);
  M5.Lcd.print("!!Hello CO2 sensor!!");

  static int ppm = 0;
  int is_get_ppm_packet = 0;
  HTTPClient http;

  struct tm timeinfo;
  time_t epoch;
  getLocalTime(&timeinfo);
  epoch = mktime(&timeinfo);
  M5.Lcd.setCursor(10, 20);
  M5.Lcd.printf("%04d-%02d-%02d ", timeinfo.tm_year+1900, timeinfo.tm_mon, timeinfo.tm_mday);
  M5.Lcd.printf("%02d:%02d:%02d ",timeinfo.tm_hour,timeinfo.tm_min,timeinfo.tm_sec);
  M5.Lcd.printf("%d", epoch);

  char buf[20];
  M5.Lcd.setCursor(10, 100);
  M5.Lcd.printf("debug:");

  if (Serial2.available()) {
    M5.Lcd.setCursor(10, 100);
    M5.Lcd.printf("debug: available");

    int tmp_ppm = 0;
    for (int i=0; i<20; i++) {
      buf[i] = Serial2.read();
      // M5.Lcd.printf(" 0x%x", buf[i]);
      if (buf[i] == 0x0a) {
        is_get_ppm_packet = 1;
        break;
      }
    }

    if (is_get_ppm_packet) {
      // get ppm value
      for (int j=0; j<6; j++) {
        if (buf[j] != 0x20) {
          tmp_ppm += (pow(10, (-1*(j-5)))) * (buf[j] - 0x30);
        }
      }

      // check SP(0x20),p(0x70),p(0x70),m(0x6d)
      if (buf[6] == 0x20 && buf[7] == 0x70 && buf[8] == 0x70 && buf[9] == 0x6d) {
        // valid
        ppm = tmp_ppm;
      }
    }

    postServer(ppm, epoch);

    M5.Lcd.setCursor(10, 60);
    M5.Lcd.printf("CO2: %5d[ppm]", ppm);
  } else {
    // M5.Lcd.setCursor(10, 100);
    // M5.Lcd.printf("debug: not available");
  }

  if (M5.BtnA.wasPressed() || M5.BtnB.wasPressed() || M5.BtnC.wasPressed()) {
    if (lcdOnOff == 0) {
      lcdOn();
    } else {
      lcdOff();
    }

    lcdOnOff = !lcdOnOff;
  }

  delay(1000);  // 1000=1sec
}
