# CO2 monitor with M5Stack and S-300L-3V

![m5stack s-300l-3v](./images/m5stack_s300.jpg)
![influxdb-dashboard](./images/influxdb-dashboard.png)

## Requirements
* [M5Stack](http://m5stack.com/)
* [S-300L-3V](https://www.switch-science.com/catalog/3685/)
* InfluxDB >= 2.0

## Usage

### UART

| M5Stack | S-300L-3V |
|---------|-----------|
|   R2    | TxD       |
| T2      | RxD       |
| 3V3     | VCC       |
| G       | GND       |


